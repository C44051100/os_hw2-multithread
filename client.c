#include "client.h"

void error_cli(const char *msg)
{
    perror(msg);
    exit(0);
}

void *child(void *data)
{
    //char *str=(char*)data;
    //printf("%s\n",str);
    pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
    //get arg from cml
    strcpy(localhost,argv[2]);
    strcpy(port,argv[4]);

    //set socket
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    char buffer[256];

    portno = atof(port);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        error_cli("ERROR opening socket");
    server = gethostbyname(localhost);
    if (server == NULL)
    {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    serv_addr.sin_family = AF_INET;
    memcpy((char *)&serv_addr.sin_addr.s_addr,(char *)server->h_addr_list[0],server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)error_cli("ERROR connecting");

    while(1)
    {
        count=0;
        //input string for searching
        fgets(input,256,stdin);
        int i=0;
        for(i=0; i<strlen(input); i++)
        {
            if(input[i]=='\"')count++;
        }

        char send[count/2][256];
        memset(send,0,sizeof(send));
        int num_space=0;
        int num_string=0;
        int quata=0;
        for(i=0; i<strlen(input); i++)
        {
            if(input[i]=='\"')
            {
                quata++;
                num_space++;
                if(quata%2==1)num_string=0;
                continue;
            }
            if(quata%2==1 || input[i]=='\n')
            {
                send[(quata-1)/2][num_string]+=input[i];
                num_string++;
            }
        }

        pthread_t t;
        for(i=0; i<count/2; i++)
        {
            pthread_create(&t,NULL,child,send[i]);
        }

        //repeat sending string to server according to string number
        for(i=0; i<count/2; i++)
        {
            memset(buffer,0,sizeof(buffer));
            sprintf(buffer,"%s",send[i]);
            if(write(sockfd,buffer,256)<0)error_cli("Error writing to socket");
        }

        char sendback[100][100];
        memset(sendback,0,sizeof(memset));

        for(i=0; i<100; i++)
        {
            read(sockfd,sendback[i],100);
        }

        for(i=0; i<100; i++)
        {
            //write(newsockfd,sendback[i],100);
            if(strlen(sendback[i])==0)break;
            printf("%s\n",sendback[i]);
        }

        pthread_join(t,NULL);
    }


    return 0;
}