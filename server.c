#include "server.h"

void error_ser(const char *msg)
{
    perror(msg);
    exit(1);
}
//work thread
void *child(void *data)
{
    while(1)
    {
        //get string to search from queue
        pthread_mutex_lock(&mutex1);
        char work[256];

        if(strlen(search[0])==0)
        {
            break;
        }
        if(strlen(search[0])!=0)
        {
            strcpy(work,search[0]);
            int j;
            for(j=1; j<10; j++)
            {
                if(strlen(search[j])!=0)strcpy(search[j-1],search[j]);
                else
                {
                    strcpy(search[j-1],"");
                    break;
                }
            }
        }
        pthread_mutex_unlock(&mutex1);

        //start searching
        int result=0;
        char answer[10][100];
        memset(answer,0,sizeof(answer));
        strcpy(answer[0],"String: \"");
        strcat(answer[0],work);
        strcat(answer[0],"\"");

        search_path(path,answer,work);

        //put answer in snedback array
        pthread_mutex_lock(&mutex1);
        int j;
        for(j=0; j<100; j++)
        {
            if(strlen(sendback[j])==0)break;
        }
        int k;
        for(k=0; k<10; k++)
        {
            if(strlen(answer[k])==0)break;
            strcpy(sendback[j+k],answer[k]);
        }
        pthread_mutex_unlock(&mutex1);
    }

    pthread_mutex_unlock(&mutex1);

    pthread_exit(NULL);
}

//search all folder and file with input path
int search_path(char* path_f,char answer[10][100],char* word)
{
    DIR* FD;
    struct dirent* in_file;
    if (NULL == (FD = opendir (path_f)))
    {
        //fprintf(stderr, "Error : Failed to open input directory - %s\n", strerror(errno));
        FILE *ofp=fopen(path_f,"r");
        char compare[strlen(word)];
        memset(compare,0,sizeof(compare));
        char c;
        int result=0;
        while(c!=EOF)
        {

            c=fgetc(ofp);
            //printf("%c\n",c);
            int j;
            for(j=0; j<strlen(word)-1; j++)
            {
                compare[j]=compare[j+1];
            }
            compare[strlen(word)-1]=c;
            int same=0;
            for(j=0; j<strlen(word); j++)
            {
                if(word[j]!=compare[j])same=1;
            }
            if(same==0)result++;
        }
        int j=0;
        for(j=1; j<10; j++)
        {
            if(strlen(answer[j])==0)break;
        }
        char s_result[100];
        sprintf(s_result,"%d",result);
        strcpy(answer[j],"FILE: ");
        strcat(answer[j],path_f);
        strcat(answer[j],", Count:");
        strcat(answer[j],s_result);
        //printf("%d",result);
        fclose(ofp);
        return result;
    }
    while ((in_file = readdir(FD)))
    {
        //printf("%s\n",in_file->d_name);

        if(strcmp(in_file->d_name,".")!=0 && strcmp(in_file->d_name,"..")!=0)
        {
            char path_tmp[100];

            strcpy(path_tmp,path_f);
            strcat(path_tmp,"/");
            strcat(path_tmp,in_file->d_name);
            //printf("%s\n",path_tmp);

            search_path(path_tmp,answer,word);
        }
    }
    return 0;
}

int main(int argc, char *argv[])
{
    strcpy(port,argv[4]);
    strcpy(thread_num,argv[6]);

    socklen_t clilen;
    char buffer[256];   //a buffer for communication
    struct sockaddr_in serv_addr, cli_addr;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        error_ser("ERROR opening socket");

    portno = atof(port);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);
    if (bind(sockfd, (struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
        error_ser("ERROR on binding");
    listen(sockfd,5);
    clilen = sizeof(cli_addr);
    newsockfd = accept(sockfd,(struct sockaddr *) &cli_addr,&clilen);
    if (newsockfd < 0)error_ser("ERROR on accept");


    while(1)
    {
        n=0;
        i=0;
        memset(search,0,sizeof(search));
        while(1)
        {
            memset(buffer,0,sizeof(buffer));
            int file_buffer_size=0;
            read(newsockfd,buffer,256);

            int num=0;
            for(i=0; i<strlen(buffer); i++)
            {

                if(buffer[i]=='\"' || buffer[i]=='\n')continue;
                if(buffer[i]!='\"')search[n][num]+=buffer[i];
                num++;
            }
            n++;

            if(buffer[strlen(buffer)-1]=='\n')break;
        }

        int string_num=0;
        printf("Query ");
        while(1)
        {
            if(strlen(search[string_num])==0)break;
            printf("\"%s\" ",search[string_num]);
            string_num++;
        }
        printf("\n");

        //int string_num=0;
        int thread_num_i=atoi(thread_num);

        memset(path,0,sizeof(path));
        strcpy(path,"./");
        strcat(path,argv[2]);

        memset(sendback,0,sizeof(sendback));

        pthread_t t[thread_num_i];//declare pthread variable
        for(i=0; i<thread_num_i; i++)
        {
            pthread_create(&t[i],NULL,child,"");
        }

        for(i=0; i<thread_num_i; i++)
        {
            pthread_join(t[i],NULL);
        }

        //send answer to client
        for(i=0; i<100; i++)
        {
            write(newsockfd,sendback[i],100);
            //if(strlen(sendback[i])==0)break;
            //printf("%s\n",sendback[i]);
        }
    }



    return 0;
}