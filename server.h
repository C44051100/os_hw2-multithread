#ifndef SERVER_H
#define SERVER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <asm/types.h>
#include <sys/socket.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <errno.h>
#include <time.h>
#include <dirent.h>

#define ERR_EXIT(m) \
        do \
        { \
                perror(m); \
                exit(EXIT_FAILURE); \
        } while(0)


char buf[1000];
char path[100];
char port[10];
char thread_num[5];
int sockfd;
int newsockfd;
int  portno;

int n=0;
int i=0;
int search_path(char* a,char answer[10][100],char* word);

char search[10][256];
char sendback[100][100];

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;



#endif